import './App.css';
import Home from './Home'
import {
  Link
} from "react-router-dom";

function App(props) {

  // id


  return (
   
    <div className="App">
     
            <h1>Hello Home !!!</h1>
            <Link to="/clock">Clock Demo</Link><br/>
            <Link to="/nameForm">NameForm Demo</Link> <br/>
            <Link to="/login">Login Demo</Link> <br/>
            <Link to="/conditional">Conditional Rendering Demo</Link>
            <Link to="/uncontrolled">Uncontrolled Component Demo</Link>
           <Home />
    </div>
    
  );
}

export default App;
