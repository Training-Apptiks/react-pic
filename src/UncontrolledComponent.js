import React from 'react'
function UncontrolledComponent(props){


    const input  = React.createRef();

    const handleSubmit = () => {

       // alert(input.current.value);
        alert(document.getElementById('name').value);

    }

return(
    <div>
        <h1>From uncontrolled component</h1>


        <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input type="text" id="name" ref={input} />
        </label>
        <input type="submit" value="Submit" />
      </form>

      

    </div>
)

}

export default UncontrolledComponent;