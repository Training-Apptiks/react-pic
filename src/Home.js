import Clock from "./Clock";
import {
    Switch,
    Route,
    Link
} from "react-router-dom";
import NameForm from "./NameForm";
import Login from "./Login";
import ConditionalRendering from './CondtionalRender'
import UncontrolledComponent from './UncontrolledComponent'
function Home(props) {

    return (
        <>
           
            <Switch>
                <Route path="/clock">
                    <Clock />
                </Route>
                <Route path="/nameForm">
                    <NameForm />
                </Route>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="/conditional">
                    <ConditionalRendering isLoggedIn="false"/>
                </Route>
                <Route path="/uncontrolled">
                    <UncontrolledComponent />
                </Route>
            </Switch>

        </>
    )

}
export default Home;