function ConditionalRender(props){

      const UserGreeting = () => {
        return <h1>Hello User !!!</h1>
       }
       const GuestGreeting = () => {
        return <h1>Hello Guest !!!</h1>
       }
       
       function Greeting() {
        const isLoggedIn = props.isLoggedIn;
        alert(isLoggedIn);
        alert(typeof isLoggedIn)


        if (isLoggedIn=="true") {
            
          return <UserGreeting />;
        }
        return <GuestGreeting />;
       }

       return(
           <>
           <h2>From</h2>
           <Greeting/>
           </>
       )
       
}
export default ConditionalRender;