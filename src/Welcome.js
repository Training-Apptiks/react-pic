import React from 'react';
class Welcome extends React.Component {
    state = {
        name: 'Rajesh',
        date: new Date()
    }
    constructor(props) {
        super(props);
        this.wish = this.wish.bind(this);
    }

    componentDidMount() {
       // alert('from componentDidMount');
        //api calls
    }
  
    componentWillUnmount() {
       // alert('from componentWillUnmount');
    }
  
    wish(){
        //alert('hello !!!' + this.state.name);
        
        //alert(this.state.name);

       var greet =  this.Greet();
       this.setState({name: greet +' Mahesh'})
    }

    Greet() {
        return "Good Morning"
    }
    render() {
        return (
            <>
                <h1>Hello World !!! {this.state.name}</h1>
                <button onClick={this.wish}>Greet</button>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>

            </>
        )
    }
}
export default Welcome;